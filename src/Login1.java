import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

//ログイン試作用　Jフレームクラスの継承と、クリック時に動作するメソッドのインターフェース
public class Login1 extends JFrame implements ActionListener{

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		//Jフレームを作る
		Login1 frame = new Login1("ログイン");
		//フレームを見える化
		frame.setVisible(true);

	}
//	コンストラクタ
	Login1(String title){
		//フレームタイトル
		setTitle(title);
		//フレームの大きさ
		setBounds(100,100,400,300);
		//×でプログラムも終了するよう初期化
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//レイアウト
		setLayout(new FlowLayout());

		//パネル１にラベル2つ、テキストボックス１、パスワード入力ボックス１
		JPanel p1 = new JPanel();
		p1.setPreferredSize(new Dimension(300,150));

		JLabel label1 = new JLabel("社員ID：");
		JTextField text1 = new JTextField(15);


		JLabel label2 = new JLabel("パスワード：");
		JPasswordField pass1 = new JPasswordField(15);

		p1.add(label1);
		p1.add(text1);
		p1.add(label2);
		p1.add(pass1);


		Container contentPane = getContentPane();
		contentPane.add(p1, BorderLayout.CENTER);

		//パネル2にボタンを配置し、クリックするとメソッド
		JPanel p2 = new JPanel();
		p2.setPreferredSize(new Dimension(300,50));

		JButton btn = new JButton("ログイン");
	    btn.addActionListener(this);

	    p2.add(btn);
		Container contentPane2 = getContentPane();
		contentPane2.add(p2, BorderLayout.SOUTH);
	  }

		//ボタンクリック時のアクション
	  public void actionPerformed(ActionEvent e){
	    JLabel label = new JLabel("クリックしたら値を取ってデータベースに接続");
	    JOptionPane.showMessageDialog(this, label);
	  }






}