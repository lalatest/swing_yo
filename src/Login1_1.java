import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class Login1_1 extends JFrame implements ActionListener{

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		Login1_1 frame = new Login1_1("ログイン試作１");
		frame.setVisible(true);

	}

	Login1_1(String title){
		setTitle(title);
		setBounds(100,100,400,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


		setLayout(new FlowLayout());


		JPanel p1 = new JPanel();
		p1.setBackground(Color.YELLOW);
		p1.setPreferredSize(new Dimension(300,150));

		JLabel label1 = new JLabel("社員ID：");
		JTextField text1 = new JTextField(15);


		JLabel label2 = new JLabel("パスワード：");
		JPasswordField pass1 = new JPasswordField(15);

		p1.add(label1);
		p1.add(text1);
		p1.add(label2);
		p1.add(pass1);

		Container contentPane = getContentPane();
		contentPane.add(p1, BorderLayout.CENTER);

		JPanel p2 = new JPanel();
		p2.setBackground(Color.BLUE);
		p2.setPreferredSize(new Dimension(300,50));

		JButton btn = new JButton("ログイン");
	    btn.addActionListener(this);

	    p2.add(btn);
		Container contentPane2 = getContentPane();
		contentPane2.add(p2, BorderLayout.SOUTH);
	  }

	  public void actionPerformed(ActionEvent e){
	    JLabel label = new JLabel("Push A Button");
	    JOptionPane.showMessageDialog(this, label);
	  }






}
